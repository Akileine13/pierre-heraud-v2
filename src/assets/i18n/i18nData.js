/* eslint-disable */

export default{
  fr: {
    title: {
      title: "Pierre Héraud - Développeur web"
    },
    header: {
      webDeveloper: "Développeur web",
      discoverMyUniverse: "Découvre mon univers"
    },
    test: "coucoux"
  },
  en: {
    title: {
      title: "Pierre Héraud - Web developer"
    },
    header: {
      webDeveloper: "Web developer",
      discoverMyUniverse: "Discover my universe"
    },
    test: "coucou"
  }
};
