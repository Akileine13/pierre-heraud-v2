import Vue from "vue";
import VueI18n from "vue-i18n";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import { BootstrapVue } from "bootstrap-vue";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import LottieVuePlayer from "@lottiefiles/vue-lottie-player";
import translate from "./assets/i18n/i18nData";

Vue.use(BootstrapVue);
Vue.use(LottieVuePlayer);
Vue.use(VueI18n);
const i18n = new VueI18n({
  locale: localStorage.getItem("lang") ?? "fr",
  fallbackLocale: "en",
  messages: translate,
});

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  i18n,
  render: (h) => h(App),
}).$mount("#app");
